var network_error = false;
var connect_error = true;
var whitelist_error = false;
var provider = null;
var contract = null;
var walletAccounts = null;
var whitelistAddresses = [
  "0x6F28579706a4673093BAbb04AB1910884c60e24D",
  "0x095C83EDF7632C36749eD105Db9a60784730D4D6",
  "0x113690e7e9a30FeD08eb1C3128596c272249db8b",
  "0x50ae434d41Bd3d4c372F95Ab8Db96081896B0f44",
  "0x447E77Cb651Ba486D59E3a024D2793f6c6B96510"
];

var CollectionConfig = {
  testnet: {
    chainId: 4,
    blockExplorer: {
      name: 'Etherscan',
      generateContractUrl: (contractAddress) => `https://rinkeby.etherscan.io/address/${contractAddress}`,
    },
  },
  mainnet: {
    chainId: 1,
    blockExplorer: {
      name: 'Etherscan',
      generateContractUrl: (contractAddress) => `https://etherscan.io/address/${contractAddress}`,
    },
  },
  contractName: 'VikitaNftToken',
  tokenName: 'Vikita NFT Token',
  tokenSymbol: 'VIK',
  hiddenMetadataUri: 'ipfs://QmWJPygR9TXdmXvycwLg1jk5cp6uJxRnzHmPunNxtm8JT3/hidden.json',
  maxSupply: 10000,
  whitelistSale: {
    price: 0.001,
    maxMintAmountPerTx: 3,
  },
  preSale: {
    price: 0.07,
    maxMintAmountPerTx: 2,
  },
  publicSale: {
    price: 0.09,
    maxMintAmountPerTx: 5,
  },
  contractAddress: "0x3D22d15817aCc882d583DeA347C2d4Ee1F5308Ab",
  marketplaceIdentifier: 'vikita',
  marketplaceConfig: {
    name: 'OpenSea',
    generateCollectionUrl: (marketplaceIdentifier, isMainnet) => 'https://' + (isMainnet ? 'www' : 'testnets') + '.opensea.io/collection/' + marketplaceIdentifier,
  },
  whitelistAddresses: whitelistAddresses,
};

var networkConfig = null;
var ContractAbi = [
  {
    "inputs": [
      {
        "internalType": "string",
        "name": "_tokenName",
        "type": "string"
      },
      {
        "internalType": "string",
        "name": "_tokenSymbol",
        "type": "string"
      },
      {
        "internalType": "uint256",
        "name": "_cost",
        "type": "uint256"
      },
      {
        "internalType": "uint256",
        "name": "_maxSupply",
        "type": "uint256"
      },
      {
        "internalType": "uint256",
        "name": "_maxMintAmountPerTx",
        "type": "uint256"
      },
      {
        "internalType": "string",
        "name": "_hiddenMetadataUri",
        "type": "string"
      }
    ],
    "stateMutability": "nonpayable",
    "type": "constructor"
  },
  {
    "inputs": [],
    "name": "ApprovalCallerNotOwnerNorApproved",
    "type": "error"
  },
  {
    "inputs": [],
    "name": "ApprovalQueryForNonexistentToken",
    "type": "error"
  },
  {
    "inputs": [],
    "name": "ApprovalToCurrentOwner",
    "type": "error"
  },
  {
    "inputs": [],
    "name": "ApproveToCaller",
    "type": "error"
  },
  {
    "inputs": [],
    "name": "BalanceQueryForZeroAddress",
    "type": "error"
  },
  {
    "inputs": [],
    "name": "MintToZeroAddress",
    "type": "error"
  },
  {
    "inputs": [],
    "name": "MintZeroQuantity",
    "type": "error"
  },
  {
    "inputs": [],
    "name": "OwnerQueryForNonexistentToken",
    "type": "error"
  },
  {
    "inputs": [],
    "name": "TransferCallerNotOwnerNorApproved",
    "type": "error"
  },
  {
    "inputs": [],
    "name": "TransferFromIncorrectOwner",
    "type": "error"
  },
  {
    "inputs": [],
    "name": "TransferToNonERC721ReceiverImplementer",
    "type": "error"
  },
  {
    "inputs": [],
    "name": "TransferToZeroAddress",
    "type": "error"
  },
  {
    "anonymous": false,
    "inputs": [
      {
        "indexed": true,
        "internalType": "address",
        "name": "owner",
        "type": "address"
      },
      {
        "indexed": true,
        "internalType": "address",
        "name": "approved",
        "type": "address"
      },
      {
        "indexed": true,
        "internalType": "uint256",
        "name": "tokenId",
        "type": "uint256"
      }
    ],
    "name": "Approval",
    "type": "event"
  },
  {
    "anonymous": false,
    "inputs": [
      {
        "indexed": true,
        "internalType": "address",
        "name": "owner",
        "type": "address"
      },
      {
        "indexed": true,
        "internalType": "address",
        "name": "operator",
        "type": "address"
      },
      {
        "indexed": false,
        "internalType": "bool",
        "name": "approved",
        "type": "bool"
      }
    ],
    "name": "ApprovalForAll",
    "type": "event"
  },
  {
    "anonymous": false,
    "inputs": [
      {
        "indexed": true,
        "internalType": "address",
        "name": "previousOwner",
        "type": "address"
      },
      {
        "indexed": true,
        "internalType": "address",
        "name": "newOwner",
        "type": "address"
      }
    ],
    "name": "OwnershipTransferred",
    "type": "event"
  },
  {
    "anonymous": false,
    "inputs": [
      {
        "indexed": true,
        "internalType": "address",
        "name": "from",
        "type": "address"
      },
      {
        "indexed": true,
        "internalType": "address",
        "name": "to",
        "type": "address"
      },
      {
        "indexed": true,
        "internalType": "uint256",
        "name": "tokenId",
        "type": "uint256"
      }
    ],
    "name": "Transfer",
    "type": "event"
  },
  {
    "inputs": [
      {
        "internalType": "address",
        "name": "to",
        "type": "address"
      },
      {
        "internalType": "uint256",
        "name": "tokenId",
        "type": "uint256"
      }
    ],
    "name": "approve",
    "outputs": [],
    "stateMutability": "nonpayable",
    "type": "function"
  },
  {
    "inputs": [
      {
        "internalType": "address",
        "name": "owner",
        "type": "address"
      }
    ],
    "name": "balanceOf",
    "outputs": [
      {
        "internalType": "uint256",
        "name": "",
        "type": "uint256"
      }
    ],
    "stateMutability": "view",
    "type": "function"
  },
  {
    "inputs": [],
    "name": "cost",
    "outputs": [
      {
        "internalType": "uint256",
        "name": "",
        "type": "uint256"
      }
    ],
    "stateMutability": "view",
    "type": "function"
  },
  {
    "inputs": [
      {
        "internalType": "uint256",
        "name": "tokenId",
        "type": "uint256"
      }
    ],
    "name": "getApproved",
    "outputs": [
      {
        "internalType": "address",
        "name": "",
        "type": "address"
      }
    ],
    "stateMutability": "view",
    "type": "function"
  },
  {
    "inputs": [],
    "name": "hiddenMetadataUri",
    "outputs": [
      {
        "internalType": "string",
        "name": "",
        "type": "string"
      }
    ],
    "stateMutability": "view",
    "type": "function"
  },
  {
    "inputs": [
      {
        "internalType": "address",
        "name": "owner",
        "type": "address"
      },
      {
        "internalType": "address",
        "name": "operator",
        "type": "address"
      }
    ],
    "name": "isApprovedForAll",
    "outputs": [
      {
        "internalType": "bool",
        "name": "",
        "type": "bool"
      }
    ],
    "stateMutability": "view",
    "type": "function"
  },
  {
    "inputs": [],
    "name": "maxMintAmountPerTx",
    "outputs": [
      {
        "internalType": "uint256",
        "name": "",
        "type": "uint256"
      }
    ],
    "stateMutability": "view",
    "type": "function"
  },
  {
    "inputs": [],
    "name": "maxSupply",
    "outputs": [
      {
        "internalType": "uint256",
        "name": "",
        "type": "uint256"
      }
    ],
    "stateMutability": "view",
    "type": "function"
  },
  {
    "inputs": [],
    "name": "merkleRoot",
    "outputs": [
      {
        "internalType": "bytes32",
        "name": "",
        "type": "bytes32"
      }
    ],
    "stateMutability": "view",
    "type": "function"
  },
  {
    "inputs": [
      {
        "internalType": "uint256",
        "name": "_mintAmount",
        "type": "uint256"
      }
    ],
    "name": "mint",
    "outputs": [],
    "stateMutability": "payable",
    "type": "function"
  },
  {
    "inputs": [
      {
        "internalType": "uint256",
        "name": "_mintAmount",
        "type": "uint256"
      },
      {
        "internalType": "address",
        "name": "_receiver",
        "type": "address"
      }
    ],
    "name": "mintForAddress",
    "outputs": [],
    "stateMutability": "nonpayable",
    "type": "function"
  },
  {
    "inputs": [],
    "name": "name",
    "outputs": [
      {
        "internalType": "string",
        "name": "",
        "type": "string"
      }
    ],
    "stateMutability": "view",
    "type": "function"
  },
  {
    "inputs": [],
    "name": "owner",
    "outputs": [
      {
        "internalType": "address",
        "name": "",
        "type": "address"
      }
    ],
    "stateMutability": "view",
    "type": "function"
  },
  {
    "inputs": [
      {
        "internalType": "uint256",
        "name": "tokenId",
        "type": "uint256"
      }
    ],
    "name": "ownerOf",
    "outputs": [
      {
        "internalType": "address",
        "name": "",
        "type": "address"
      }
    ],
    "stateMutability": "view",
    "type": "function"
  },
  {
    "inputs": [],
    "name": "paused",
    "outputs": [
      {
        "internalType": "bool",
        "name": "",
        "type": "bool"
      }
    ],
    "stateMutability": "view",
    "type": "function"
  },
  {
    "inputs": [],
    "name": "renounceOwnership",
    "outputs": [],
    "stateMutability": "nonpayable",
    "type": "function"
  },
  {
    "inputs": [],
    "name": "revealed",
    "outputs": [
      {
        "internalType": "bool",
        "name": "",
        "type": "bool"
      }
    ],
    "stateMutability": "view",
    "type": "function"
  },
  {
    "inputs": [
      {
        "internalType": "address",
        "name": "from",
        "type": "address"
      },
      {
        "internalType": "address",
        "name": "to",
        "type": "address"
      },
      {
        "internalType": "uint256",
        "name": "tokenId",
        "type": "uint256"
      }
    ],
    "name": "safeTransferFrom",
    "outputs": [],
    "stateMutability": "nonpayable",
    "type": "function"
  },
  {
    "inputs": [
      {
        "internalType": "address",
        "name": "from",
        "type": "address"
      },
      {
        "internalType": "address",
        "name": "to",
        "type": "address"
      },
      {
        "internalType": "uint256",
        "name": "tokenId",
        "type": "uint256"
      },
      {
        "internalType": "bytes",
        "name": "_data",
        "type": "bytes"
      }
    ],
    "name": "safeTransferFrom",
    "outputs": [],
    "stateMutability": "nonpayable",
    "type": "function"
  },
  {
    "inputs": [
      {
        "internalType": "address",
        "name": "operator",
        "type": "address"
      },
      {
        "internalType": "bool",
        "name": "approved",
        "type": "bool"
      }
    ],
    "name": "setApprovalForAll",
    "outputs": [],
    "stateMutability": "nonpayable",
    "type": "function"
  },
  {
    "inputs": [
      {
        "internalType": "uint256",
        "name": "_cost",
        "type": "uint256"
      }
    ],
    "name": "setCost",
    "outputs": [],
    "stateMutability": "nonpayable",
    "type": "function"
  },
  {
    "inputs": [
      {
        "internalType": "string",
        "name": "_hiddenMetadataUri",
        "type": "string"
      }
    ],
    "name": "setHiddenMetadataUri",
    "outputs": [],
    "stateMutability": "nonpayable",
    "type": "function"
  },
  {
    "inputs": [
      {
        "internalType": "uint256",
        "name": "_maxMintAmountPerTx",
        "type": "uint256"
      }
    ],
    "name": "setMaxMintAmountPerTx",
    "outputs": [],
    "stateMutability": "nonpayable",
    "type": "function"
  },
  {
    "inputs": [
      {
        "internalType": "bytes32",
        "name": "_merkleRoot",
        "type": "bytes32"
      }
    ],
    "name": "setMerkleRoot",
    "outputs": [],
    "stateMutability": "nonpayable",
    "type": "function"
  },
  {
    "inputs": [
      {
        "internalType": "bool",
        "name": "_state",
        "type": "bool"
      }
    ],
    "name": "setPaused",
    "outputs": [],
    "stateMutability": "nonpayable",
    "type": "function"
  },
  {
    "inputs": [
      {
        "internalType": "bool",
        "name": "_state",
        "type": "bool"
      }
    ],
    "name": "setRevealed",
    "outputs": [],
    "stateMutability": "nonpayable",
    "type": "function"
  },
  {
    "inputs": [
      {
        "internalType": "string",
        "name": "_uriPrefix",
        "type": "string"
      }
    ],
    "name": "setUriPrefix",
    "outputs": [],
    "stateMutability": "nonpayable",
    "type": "function"
  },
  {
    "inputs": [
      {
        "internalType": "string",
        "name": "_uriSuffix",
        "type": "string"
      }
    ],
    "name": "setUriSuffix",
    "outputs": [],
    "stateMutability": "nonpayable",
    "type": "function"
  },
  {
    "inputs": [
      {
        "internalType": "bool",
        "name": "_state",
        "type": "bool"
      }
    ],
    "name": "setWhitelistMintEnabled",
    "outputs": [],
    "stateMutability": "nonpayable",
    "type": "function"
  },
  {
    "inputs": [
      {
        "internalType": "bytes4",
        "name": "interfaceId",
        "type": "bytes4"
      }
    ],
    "name": "supportsInterface",
    "outputs": [
      {
        "internalType": "bool",
        "name": "",
        "type": "bool"
      }
    ],
    "stateMutability": "view",
    "type": "function"
  },
  {
    "inputs": [],
    "name": "symbol",
    "outputs": [
      {
        "internalType": "string",
        "name": "",
        "type": "string"
      }
    ],
    "stateMutability": "view",
    "type": "function"
  },
  {
    "inputs": [
      {
        "internalType": "uint256",
        "name": "_tokenId",
        "type": "uint256"
      }
    ],
    "name": "tokenURI",
    "outputs": [
      {
        "internalType": "string",
        "name": "",
        "type": "string"
      }
    ],
    "stateMutability": "view",
    "type": "function"
  },
  {
    "inputs": [],
    "name": "totalSupply",
    "outputs": [
      {
        "internalType": "uint256",
        "name": "",
        "type": "uint256"
      }
    ],
    "stateMutability": "view",
    "type": "function"
  },
  {
    "inputs": [
      {
        "internalType": "address",
        "name": "from",
        "type": "address"
      },
      {
        "internalType": "address",
        "name": "to",
        "type": "address"
      },
      {
        "internalType": "uint256",
        "name": "tokenId",
        "type": "uint256"
      }
    ],
    "name": "transferFrom",
    "outputs": [],
    "stateMutability": "nonpayable",
    "type": "function"
  },
  {
    "inputs": [
      {
        "internalType": "address",
        "name": "newOwner",
        "type": "address"
      }
    ],
    "name": "transferOwnership",
    "outputs": [],
    "stateMutability": "nonpayable",
    "type": "function"
  },
  {
    "inputs": [],
    "name": "uriPrefix",
    "outputs": [
      {
        "internalType": "string",
        "name": "",
        "type": "string"
      }
    ],
    "stateMutability": "view",
    "type": "function"
  },
  {
    "inputs": [],
    "name": "uriSuffix",
    "outputs": [
      {
        "internalType": "string",
        "name": "",
        "type": "string"
      }
    ],
    "stateMutability": "view",
    "type": "function"
  },
  {
    "inputs": [
      {
        "internalType": "address",
        "name": "_owner",
        "type": "address"
      }
    ],
    "name": "walletOfOwner",
    "outputs": [
      {
        "internalType": "uint256[]",
        "name": "",
        "type": "uint256[]"
      }
    ],
    "stateMutability": "view",
    "type": "function"
  },
  {
    "inputs": [
      {
        "internalType": "address",
        "name": "",
        "type": "address"
      }
    ],
    "name": "whitelistClaimed",
    "outputs": [
      {
        "internalType": "bool",
        "name": "",
        "type": "bool"
      }
    ],
    "stateMutability": "view",
    "type": "function"
  },
  {
    "inputs": [
      {
        "internalType": "uint256",
        "name": "_mintAmount",
        "type": "uint256"
      },
      {
        "internalType": "bytes32[]",
        "name": "_merkleProof",
        "type": "bytes32[]"
      }
    ],
    "name": "whitelistMint",
    "outputs": [],
    "stateMutability": "payable",
    "type": "function"
  },
  {
    "inputs": [],
    "name": "whitelistMintEnabled",
    "outputs": [
      {
        "internalType": "bool",
        "name": "",
        "type": "bool"
      }
    ],
    "stateMutability": "view",
    "type": "function"
  },
  {
    "inputs": [],
    "name": "withdraw",
    "outputs": [],
    "stateMutability": "nonpayable",
    "type": "function"
  }
];

var currentTime = moment().tz("Europe/Moscow");
var startTime = moment("2022-03-19 10:00:00").tz("Europe/Moscow");
var mintData = {
  bought: 0,
  currentPrice: 0,
  currentMaxCount: 0,
  stages: [
    {
      id: "#firstwave",
      needWhiteList: true,
      start: startTime,
      end: moment(startTime).add(5, 'days'),
      title: 'Whitelist Premium Mint',
      waves: [
        {
          title: 'I',
          supply: 100,
          start: startTime,
          end: moment(startTime).add(1, 'days'),
        },
        {
          title: 'II',
          supply: 100,
          start: moment(startTime).add(1, 'days'),
          end: moment(startTime).add(2, 'days'),
        },
        {
          title: 'III',
          supply: 100,
          start: moment(startTime).add(2, 'days'),
          end: moment(startTime).add(3, 'days'),
        },
        {
          title: 'IV',
          supply: 100,
          start: moment(startTime).add(3, 'days'),
          end: moment(startTime).add(4, 'days'),
        },
        {
          title: 'V',
          supply: 1100,
          start: moment(startTime).add(4, 'days'),
          end: moment(startTime).add(5, 'days'),
        }
      ],
      totalSupply: 1500,
    },
    {
      id: "#secondtwave",
      needWhiteList: true,
      start: moment(startTime).add(5, 'days'),
      end: moment(startTime).add(10, 'days'),
      title: 'Whitelist Pre Sale Mint',
      waves: [
        {
          title: 'I',
          supply: 100,
          start: moment(startTime).add(5, 'days'),
          end: moment(startTime).add(6, 'days'),
        },
        {
          title: 'II',
          supply: 100,
          start: moment(startTime).add(6, 'days'),
          end: moment(startTime).add(7, 'days'),
        },
        {
          title: 'III',
          supply: 100,
          start: moment(startTime).add(7, 'days'),
          end: moment(startTime).add(8, 'days'),
        },
        {
          title: 'IV',
          supply: 100,
          start: moment(startTime).add(8, 'days'),
          end: moment(startTime).add(9, 'days'),
        },
        {
          title: 'V',
          supply: 1100,
          start: moment(startTime).add(9, 'days'),
          end: moment(startTime).add(10, 'days'),
        }
      ],
      totalSupply: 1500,
    },
    {
      id: "#thirdwave",
      needWhiteList: false,
      start: moment(startTime).add(10, 'days'),
      end: moment(startTime).add(30, 'days'),
      title: 'Whitelist Public Mint',
      waves: [
        {
          title: '',
          supply: 7000,
          start: moment(startTime).add(10, 'days'),
          end: moment(startTime).add(30, 'days'),
        }
      ],
      totalSupply: 7000,
    }
  ]
};

var currentSelect = 1;
var isPaused = true;
var currentPriceBig = null;
var isUserInWhitelist = false;

var leafNodes = whitelistAddresses.map(addr => keccak256(addr));
var merkleTreeAddr = new MerkleTree(leafNodes, keccak256, { sortPairs: true });
var walletAccountProof = null;

function initWaves() {

  var stageRamain = 0;
  var timerData = [];

  mintData.stages.forEach(function(stage) {
    var isCirrentStage = stage.start.unix() <= startTime.unix() && stage.end.unix() >= startTime.unix();
    var isNextStage = stage.start.unix() > startTime.unix();
    var isBeforeStage = stage.end.unix() < startTime.unix();

    var block = $(stage.id);
    var currentWave = stage.waves.filter(function(wave) {
      return wave.start.unix() <= startTime.unix() && wave.end.unix() >= startTime.unix();
    });

    if (isCirrentStage) {

      if (currentWave.length) {
        var totalRamain = stageRamain;

        stage.waves.forEach(function(beforeWave) {
          if (beforeWave.start.unix() < currentWave[0].start.unix()) {
            totalRamain = totalRamain + beforeWave.supply
          }
        })

        var remain = mintData.bought - totalRamain;


        if (remain >= currentWave[0].supply) {
          isPaused = true;
        }

        if (stage.needWhiteList && !isUserInWhitelist) {
          isPaused = true;
          whitelist_error = true;
          $('#whitelist_error').addClass('display');
        }

        block.find('.wave-title').html(`${stage.title} ${currentWave[0].title}`);
        block.find('.wave-supply').html(`${remain}/${currentWave[0].supply}`);
        block.find('.wave-percent').width(`${(currentWave[0].supply / 100) * remain}%`);
        block.find('.wave-price').html(`${mintData.currentPrice} ETH`);

        if (isPaused && isUserInWhitelist) {
          block.find('.wave-count').removeClass('disabled');
          block.find('.wave-btn').removeClass('disabled');
        }

        if (!isPaused) {
          block.find('.wave-count').removeClass('disabled');
          block.find('.wave-btn').removeClass('disabled');
        }

        var counts = '';
        for (var i = 1; i <= mintData.currentMaxCount; i++) {
          counts = counts + `<option>${i}</option>`;
        }

        block.find('.wave-count').html(counts);
      }
    }

    if (isNextStage) {
      var diffTime = stage.start.unix() - currentTime.unix();
      var duration = moment.duration(diffTime * 1000, 'milliseconds');
      timerData.push({
        duration,
        id: stage.id
      });

      block.find('.wave-supply').html(`0/${stage.totalSupply}`);
      block.find('.wave-percent').width(0);
    }

    if (isBeforeStage) {
      block.find('.wave-start').html('');
      block.find('.wave-supply').html(`${stage.totalSupply}/${stage.totalSupply}`);
      block.find('.wave-percent').width('100%');
    };

    stageRamain = stageRamain + stage.totalSupply;
  });

  var interval = 1000;

  setInterval(function () {
    timerData = timerData.map(function(timer) {
      var duration = moment.duration(timer.duration.asMilliseconds() - interval, 'milliseconds');
      var d = moment.duration(duration).days();
      var h = moment.duration(duration).hours();
      var m = moment.duration(duration).minutes();
      var s = moment.duration(duration).seconds();

      timer.duration = duration;
      timer.d = $.trim(d).length === 1 ? '0' + d : d;
      timer.h = $.trim(h).length === 1 ? '0' + h : h;
      timer.m = $.trim(m).length === 1 ? '0' + m : m;
      timer.s = $.trim(s).length === 1 ? '0' + s : s;

      return timer
    });

    timerData.forEach(function(timer) {
      $(timer.id).find('.wave-start').html(`Starging in <span class="days">${timer.d}</span> days <span class="hours">${timer.h}</span> hours <span class="minutes">${timer.m}</span> minutes <span class="seconds">${timer.s}</span> seconds`);
    });

  }, interval);
};

initWaves();

window.onload = async function () {

  var browserProvider = await detectEthereumProvider()
  provider = new ethers.providers.Web3Provider(browserProvider);

  if (provider) {
    initWallet();
  }
}

async function initWallet() {
  walletAccounts = await provider.listAccounts();
  if (walletAccounts.length) {
    var walletAccount = walletAccounts[0];
    var network = await provider.getNetwork();
    walletAccountProof = walletAccount;


    if (network.chainId === CollectionConfig.mainnet.chainId) {
      networkConfig = CollectionConfig.mainnet;
    } else if (network.chainId === CollectionConfig.testnet.chainId) {
      networkConfig = CollectionConfig.testnet;
    } else {
      network_error = true;
      $('#network_error').addClass('display');
    }

    var code = await provider.getCode(CollectionConfig.contractAddress);

    if (code === '0x') {
      $('#network_error').addClass('display');
    } else {
      var signer = provider.getSigner();
      contract = new ethers.Contract(
        CollectionConfig.contractAddress,
        ContractAbi,
        signer,
      )

      const accountEllipsis = `${walletAccount.substring(0, 2)}...${walletAccount.substring(walletAccount.length - 4)}`;

      $('#connect_wallet').text(`${network.name} ${accountEllipsis}`);
      network_error = false;
      connect_error = false;
      $('#network_error').removeClass('display');
      $('#connect_error').removeClass('display');
      $('#whitelist_error').removeClass('display');

      mintData.bought = (await contract.totalSupply()).toNumber();
      currentPriceBig = await contract.cost();
      mintData.currentPrice = ethers.utils.formatEther(currentPriceBig);
      mintData.currentMaxCount = (await contract.maxMintAmountPerTx()).toNumber();
      isPaused = await contract.paused();
      isUserInWhitelist = merkleTreeAddr.getLeafIndex(keccak256(walletAccount)) >= 0;

      if (isPaused && !isUserInWhitelist) {
        whitelist_error = true;
        $('#whitelist_error').addClass('display');
      }
      initWaves();
    }
  }
}

async function connectWallet() {
  try {
    await provider.provider.request({ method: 'eth_requestAccounts' });

    initWallet();
  } catch (e) {
    console.log('e', e);
  }
};

function onSelectCount(id) {
  var select = $(`#${id} .wave-count`);
  var option = select[0].options[select[0].selectedIndex];
  currentSelect = option.text;
  console.log('currentSelect', currentSelect);
}

async function onMint() {
  try {
    if (!isPaused) {
      await contract.mint(currentSelect, {value: currentPriceBig.mul(currentSelect)});
    }

    if (isPaused && isUserInWhitelist) {
      await contract.whitelistMint(currentSelect, merkleTreeAddr.getHexProof(keccak256(walletAccountProof)), {value: currentPriceBig.mul(currentSelect)});
    }

  } catch (e) {
    console.log('e', e);
  }
};
